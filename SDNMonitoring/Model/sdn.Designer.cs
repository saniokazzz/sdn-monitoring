using System.Xml.Serialization;
using System.Collections.Generic;

namespace SDNMonitoring
{
	[XmlRoot(ElementName = "publshInformation", Namespace = "http://tempuri.org/sdnList.xsd")]
	public class PublshInformation
	{
		[XmlElement(ElementName = "Publish_Date", Namespace = "http://tempuri.org/sdnList.xsd")]
		public string Publish_Date { get; set; }
		[XmlElement(ElementName = "Record_Count", Namespace = "http://tempuri.org/sdnList.xsd")]
		public int Record_Count { get; set; }
	}

	[XmlRoot(ElementName = "programList", Namespace = "http://tempuri.org/sdnList.xsd")]
	public class ProgramList
	{
		[XmlElement(ElementName = "program", Namespace = "http://tempuri.org/sdnList.xsd")]
		public List<string> Program { get; set; }
	}

	[XmlRoot(ElementName = "id", Namespace = "http://tempuri.org/sdnList.xsd")]
	public class Id
	{
		[XmlElement(ElementName = "uid", Namespace = "http://tempuri.org/sdnList.xsd")]
		public int Uid { get; set; }
        [XmlElement(ElementName = "idType", Namespace = "http://tempuri.org/sdnList.xsd")]
        public string IdType { get; set; }
		[XmlElement(ElementName = "idNumber", Namespace = "http://tempuri.org/sdnList.xsd")]
		public string IdNumber { get; set; }
        [XmlElement(ElementName = "idCountry", Namespace = "http://tempuri.org/sdnList.xsd")]
        public string IdCountry { get; set; }
		[XmlElement(ElementName = "issueDate", Namespace = "http://tempuri.org/sdnList.xsd")]
		public string IssueDate { get; set; }
		[XmlElement(ElementName = "expirationDate", Namespace = "http://tempuri.org/sdnList.xsd")]
		public string ExpirationDate { get; set; }
	}

	[XmlRoot(ElementName = "idList", Namespace = "http://tempuri.org/sdnList.xsd")]
	public class IdList
	{
		[XmlElement(ElementName = "id", Namespace = "http://tempuri.org/sdnList.xsd")]
		public Id Id { get; set; }
	}

	[XmlRoot(ElementName = "aka", Namespace = "http://tempuri.org/sdnList.xsd")]
	public class Aka
	{
		[XmlElement(ElementName = "uid", Namespace = "http://tempuri.org/sdnList.xsd")]
		public int Uid { get; set; }
		[XmlElement(ElementName = "type", Namespace = "http://tempuri.org/sdnList.xsd")]
		public string Type { get; set; }
		[XmlElement(ElementName = "category", Namespace = "http://tempuri.org/sdnList.xsd")]
		public string Category { get; set; }
		[XmlElement(ElementName = "firstName", Namespace = "http://tempuri.org/sdnList.xsd")]
		public string FirstName { get; set; }
		[XmlElement(ElementName = "lastName", Namespace = "http://tempuri.org/sdnList.xsd")]
		public string LastName { get; set; }
	}

	[XmlRoot(ElementName = "akaList", Namespace = "http://tempuri.org/sdnList.xsd")]
	public class AkaList
	{
		[XmlElement(ElementName = "aka", Namespace = "http://tempuri.org/sdnList.xsd")]
		public List<Aka> Aka { get; set; }
	}

	[XmlRoot(ElementName = "address", Namespace = "http://tempuri.org/sdnList.xsd")]
	public class Address
	{
		[XmlElement(ElementName = "uid", Namespace = "http://tempuri.org/sdnList.xsd")]
		public int Uid { get; set; }
		[XmlElement(ElementName = "address1", Namespace = "http://tempuri.org/sdnList.xsd")]
		public string Address1 { get; set; }
		[XmlElement(ElementName = "address2", Namespace = "http://tempuri.org/sdnList.xsd")]
		public string Address2 { get; set; }
        [XmlElement(ElementName = "address3", Namespace = "http://tempuri.org/sdnList.xsd")]
        public string Address3 { get; set; }
		[XmlElement(ElementName = "city", Namespace = "http://tempuri.org/sdnList.xsd")]
		public string City { get; set; }
        [XmlElement(ElementName = "stateOrProvince", Namespace = "http://tempuri.org/sdnList.xsd")]
        public string StateOrProvince { get; set; }
		[XmlElement(ElementName = "postalCode", Namespace = "http://tempuri.org/sdnList.xsd")]
		public string PostalCode { get; set; }
        [XmlElement(ElementName = "country", Namespace = "http://tempuri.org/sdnList.xsd")]
        public string Country { get; set; }
    }

	[XmlRoot(ElementName = "addressList", Namespace = "http://tempuri.org/sdnList.xsd")]
	public class AddressList
	{
		[XmlElement(ElementName = "address", Namespace = "http://tempuri.org/sdnList.xsd")]
		public List<Address> Address { get; set; }
	}

	[XmlRoot(ElementName = "nationality", Namespace = "http://tempuri.org/sdnList.xsd")]
	public class Nationality
	{
		[XmlElement(ElementName = "uid", Namespace = "http://tempuri.org/sdnList.xsd")]
		public int Uid { get; set; }
		[XmlElement(ElementName = "country", Namespace = "http://tempuri.org/sdnList.xsd")]
		public string Country { get; set; }
		[XmlElement(ElementName = "mainEntry", Namespace = "http://tempuri.org/sdnList.xsd")]
		public bool MainEntry { get; set; }
	}

	[XmlRoot(ElementName = "nationalityList", Namespace = "http://tempuri.org/sdnList.xsd")]
	public class NationalityList
	{
		[XmlElement(ElementName = "nationality", Namespace = "http://tempuri.org/sdnList.xsd")]
		public List<Nationality> Nationality { get; set; }
	}

	[XmlRoot(ElementName = "citizenship", Namespace = "http://tempuri.org/sdnList.xsd")]
	public class Citizenship
	{
		[XmlElement(ElementName = "uid", Namespace = "http://tempuri.org/sdnList.xsd")]
		public int Uid { get; set; }
		[XmlElement(ElementName = "country", Namespace = "http://tempuri.org/sdnList.xsd")]
		public string Country { get; set; }
		[XmlElement(ElementName = "mainEntry", Namespace = "http://tempuri.org/sdnList.xsd")]
		public bool MainEntry { get; set; }
	}

	[XmlRoot(ElementName = "citizenshipList", Namespace = "http://tempuri.org/sdnList.xsd")]
	public class CitizenshipList
	{
		[XmlElement(ElementName = "citizenship", Namespace = "http://tempuri.org/sdnList.xsd")]
		public List<Citizenship> Citizenship { get; set; }
	}

	[XmlRoot(ElementName = "dateOfBirthItem", Namespace = "http://tempuri.org/sdnList.xsd")]
	public class DateOfBirthItem
	{
		[XmlElement(ElementName = "uid", Namespace = "http://tempuri.org/sdnList.xsd")]
		public int Uid { get; set; }
		[XmlElement(ElementName = "dateOfBirth", Namespace = "http://tempuri.org/sdnList.xsd")]
		public string DateOfBirth { get; set; }
		[XmlElement(ElementName = "mainEntry", Namespace = "http://tempuri.org/sdnList.xsd")]
		public bool MainEntry { get; set; }
	}

	[XmlRoot(ElementName = "dateOfBirthList", Namespace = "http://tempuri.org/sdnList.xsd")]
	public class DateOfBirthList
	{
		[XmlElement(ElementName = "dateOfBirthItem", Namespace = "http://tempuri.org/sdnList.xsd")]
		public List<DateOfBirthItem> DateOfBirthItem { get; set; }
	}

	[XmlRoot(ElementName = "placeOfBirthItem", Namespace = "http://tempuri.org/sdnList.xsd")]
	public class PlaceOfBirthItem
	{
		[XmlElement(ElementName = "uid", Namespace = "http://tempuri.org/sdnList.xsd")]
		public int Uid { get; set; }
		[XmlElement(ElementName = "placeOfBirth", Namespace = "http://tempuri.org/sdnList.xsd")]
		public string PlaceOfBirth { get; set; }
		[XmlElement(ElementName = "mainEntry", Namespace = "http://tempuri.org/sdnList.xsd")]
		public bool MainEntry { get; set; }
	}

	[XmlRoot(ElementName = "placeOfBirthList", Namespace = "http://tempuri.org/sdnList.xsd")]
	public class PlaceOfBirthList
	{
		[XmlElement(ElementName = "placeOfBirthItem", Namespace = "http://tempuri.org/sdnList.xsd")]
		public List<PlaceOfBirthItem> PlaceOfBirthItem { get; set; }
	}

	[XmlRoot(ElementName = "vesselInfo", Namespace = "http://tempuri.org/sdnList.xsd")]
	public class VesselInfo
	{
		[XmlElement(ElementName = "callSign", Namespace = "http://tempuri.org/sdnList.xsd")]
		public string CallSign { get; set; }
		[XmlElement(ElementName = "vesselType", Namespace = "http://tempuri.org/sdnList.xsd")]
		public string VesselType { get; set; }
		[XmlElement(ElementName = "vesselFlag", Namespace = "http://tempuri.org/sdnList.xsd")]
		public string VesselFlag { get; set; }
		[XmlElement(ElementName = "vesselOwner", Namespace = "http://tempuri.org/sdnList.xsd")]
		public string VesselOwner { get; set; }
		[XmlElement(ElementName = "tonnage", Namespace = "http://tempuri.org/sdnList.xsd")]
		public int Tonnage { get; set; }
		[XmlElement(ElementName = "grossRegisteredTonnage", Namespace = "http://tempuri.org/sdnList.xsd")]
		public int GrossRegisteredTonnage { get; set; }
	}

	[XmlRoot(ElementName = "sdnEntry", Namespace = "http://tempuri.org/sdnList.xsd")]
	public class SdnEntry
	{
		[XmlElement(ElementName = "uid", Namespace = "http://tempuri.org/sdnList.xsd")]
		public int Uid { get; set; }
		[XmlElement(ElementName = "firstName", Namespace = "http://tempuri.org/sdnList.xsd")]
		public string FirstName { get; set; }
		[XmlElement(ElementName = "lastName", Namespace = "http://tempuri.org/sdnList.xsd")]
		public string LastName { get; set; }
		[XmlElement(ElementName = "title", Namespace = "http://tempuri.org/sdnList.xsd")]
		public string Title { get; set; }
		[XmlElement(ElementName = "sdnType", Namespace = "http://tempuri.org/sdnList.xsd")]
		public string SdnType { get; set; }
		[XmlElement(ElementName = "remarks", Namespace = "http://tempuri.org/sdnList.xsd")]
		public string Remarks { get; set; }
		[XmlElement(ElementName = "programList", Namespace = "http://tempuri.org/sdnList.xsd")]
		public ProgramList ProgramList { get; set; }
		[XmlElement(ElementName = "idList", Namespace = "http://tempuri.org/sdnList.xsd")]
		public IdList IdList { get; set; }
		[XmlElement(ElementName = "akaList", Namespace = "http://tempuri.org/sdnList.xsd")]
		public AkaList AkaList { get; set; }
		[XmlElement(ElementName = "addressList", Namespace = "http://tempuri.org/sdnList.xsd")]
		public AddressList AddressList { get; set; }
		[XmlElement(ElementName = "nationalityList", Namespace = "http://tempuri.org/sdnList.xsd")]
		public NationalityList NationalityList { get; set; }
		[XmlElement(ElementName = "citizenshipList", Namespace = "http://tempuri.org/sdnList.xsd")]
		public CitizenshipList CitizenshipList { get; set; }
		[XmlElement(ElementName = "dateOfBirthList", Namespace = "http://tempuri.org/sdnList.xsd")]
		public DateOfBirthList DateOfBirthList { get; set; }
		[XmlElement(ElementName = "placeOfBirthList", Namespace = "http://tempuri.org/sdnList.xsd")]
		public PlaceOfBirthList PlaceOfBirthList { get; set; }
		[XmlElement(ElementName = "vesselInfo", Namespace = "http://tempuri.org/sdnList.xsd")]
		public VesselInfo VesselInfo { get; set; }
		public string Name => $"{FirstName} {LastName}".TrimStart();
    }

	[XmlRoot(ElementName = "sdnList", Namespace = "http://tempuri.org/sdnList.xsd")]
	public class SdnList
	{
		[XmlElement(ElementName = "publshInformation", Namespace = "http://tempuri.org/sdnList.xsd")]
		public PublshInformation PublshInformation { get; set; }
		[XmlElement(ElementName = "sdnEntry", Namespace = "http://tempuri.org/sdnList.xsd")]
		public SdnEntry SdnEntry { get; set; }
		[XmlAttribute(AttributeName = "mstns", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Mstns { get; set; }
		[XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Xsi { get; set; }
		[XmlAttribute(AttributeName = "schemaLocation", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
		public string SchemaLocation { get; set; }
	}
}
