﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

namespace SDNMonitoring
{
    /// <summary>
    /// This class initiates XmlReader directly on SDN list XML file, and uses it to read provided document;
    /// Deserialization of whole XML isn't used intentionally, to keep better performance.
    /// </summary>
    public class SDNXmlDataSource : IEnumerable<SdnEntry>
    {
        private readonly string _filePath;
        readonly XmlSerializer _serializer = new XmlSerializer(typeof(SdnEntry));
        private readonly XmlReaderSettings _xmlReaderSettings = new XmlReaderSettings()
        {
            Async = true,
            IgnoreWhitespace = true
        };

        public SDNXmlDataSource(string filePath)
        {
            _filePath = filePath;
        }

        public IEnumerator<SdnEntry> GetEnumerator()
        {
            using var xmlReader = XmlReader.Create(_filePath, _xmlReaderSettings);
            while (xmlReader.Read())
            {
                if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == "sdnEntry")
                {
                    yield return (SdnEntry) _serializer.Deserialize(xmlReader.ReadSubtree());
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
