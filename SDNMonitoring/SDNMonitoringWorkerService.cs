﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using SDNMonitoring.Configuration;
using SDNMonitoring.Services;

namespace SDNMonitoring
{
    /// <summary>
    /// Worker service for running periodic job.
    /// </summary>
    public class SDNMonitoringWorkerService : IHostedService
    {
        private readonly IServiceProvider _services;
        private readonly SDNServiceConfiguration _sdnServiceConfiguration;
        private readonly ILogger<SDNMonitoringWorkerService> _logger;

        public SDNMonitoringWorkerService(IServiceProvider services,
            SDNServiceConfiguration sdnServiceConfiguration,
            ILogger<SDNMonitoringWorkerService> logger)
        {
            _services = services;
            _sdnServiceConfiguration = sdnServiceConfiguration;
            _logger = logger;
        }
        
        public async Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Starting SDNMonitoringWorkerService.");
            var stopwatch = new Stopwatch();

            while (!cancellationToken.IsCancellationRequested)
            {
                _logger.LogInformation("SDNMonitoringWorkerService executing job...");

                stopwatch.Start();
                using var scope = _services.CreateScope();
                using var scopedSDNMonitoringService = scope.ServiceProvider.GetRequiredService<ISDNMonitoringService>();
                await scopedSDNMonitoringService.CheckSDNForChanges(cancellationToken);
                stopwatch.Stop();

                _logger.LogInformation($"SDNMonitoringWorkerService job execution finished. Elapsed: {stopwatch.Elapsed}.");
                Thread.Sleep(TimeSpan.FromSeconds(_sdnServiceConfiguration.MonitoringIntervalSeconds));
            }
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Stopping SDNMonitoringWorkerService.");
            await Task.CompletedTask;
        }
    }
}
