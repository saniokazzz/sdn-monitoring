﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SDNMonitoring.Storage;
using SDNMonitoring.Storage.Entities;

namespace SDNMonitoring.Services
{
    /// <summary>
    /// Service, responsible for storing last SDN list XML file path and other persistent information, if required;
    /// In larger projects, one more abstraction layer would be used for database access - entities repository.
    /// For the sake of simplicity, in current solution the repository layer is omitted and the architecture is flattened.
    /// </summary>
    public interface ISystemSettingsService : IDisposable
    {
        public const string LastSDNFilePath = "LastSDNFilePath";

        Task<string> GetSettingValue(string settingKey, CancellationToken cancellationToken);
        Task SetSettingValue(string settingKey, string settingValue, CancellationToken cancellationToken);
    }

    public class SystemSettingsService : ISystemSettingsService
    {

        private readonly SDNEventsDbContext _dbContext;

        public SystemSettingsService(SDNEventsDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<string> GetSettingValue(string settingKey, CancellationToken cancellationToken)
        {
            return (await _dbContext.SystemSettings.FirstOrDefaultAsync(x => x.Id == settingKey, cancellationToken))?.Value;
        }

        public async Task SetSettingValue(string settingKey, string settingValue, CancellationToken cancellationToken)
        {
            var setting = await _dbContext.SystemSettings.FirstOrDefaultAsync(x => x.Id == settingKey, cancellationToken);
            if (setting != null)
            {
                setting.Value = settingValue;
                _dbContext.SystemSettings.Update(setting);
            }
            else
            {
                await _dbContext.SystemSettings.AddAsync(new SystemSetting(settingKey, settingValue), cancellationToken);
            }
        }

        public void Dispose()
        {
            _dbContext?.Dispose();
        }
    }
}
