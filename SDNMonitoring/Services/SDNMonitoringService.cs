﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using DeepEqual.Syntax;
using Microsoft.Extensions.Logging;
using SDNMonitoring.Configuration;
using SDNMonitoring.Storage;
using SDNMonitoring.Storage.Entities;

namespace SDNMonitoring.Services
{
    /// <summary>
    /// Service for comparing SDN lists XML and produce events from modified entries.
    /// All produced events are stored in the database. All downloaded SDN list XML files are stored for keeping track of history;
    /// Work algorithm:
    /// 1. Download SDN list XML and store it locally;
    /// 2. Compare new SDN list against latest one based on entries IDs, while iterating through new SDN list:  
    ///  2.1. Check if there are some "gaps" in the new list because of removed entries;
    ///  2.2. Check if matching entries by IDs are same by using deep comparison of objects;
    ///  2.3. Check if new list has more entries than the old one;
    /// Assumptions:
    ///  1. Entries are ordered by IDs;
    ///  2. SND list XML uses incremental IDs and all new entries have greater IDs than previous entries; 
    /// </summary>
    public interface ISDNMonitoringService : IDisposable
    {
        Task CheckSDNForChanges(CancellationToken cancellationToken);
    }

    public class SDNMonitoringService : ISDNMonitoringService
    {
        private readonly SDNServiceConfiguration _sdnServiceConfiguration;
        private readonly ISystemSettingsService _systemSettingsService;
        private readonly ILogger<SDNMonitoringService> _logger;
        private readonly SDNEventsDbContext _dbContext;
        private readonly WebClient _webClient;

        public SDNMonitoringService(SDNServiceConfiguration sdnServiceConfiguration,
            ISystemSettingsService systemSettingsService,
            ILogger<SDNMonitoringService> logger,
            SDNEventsDbContext dbContext)
        {
            _sdnServiceConfiguration = sdnServiceConfiguration;
            _systemSettingsService = systemSettingsService;
            _logger = logger;
            _dbContext = dbContext;
            _webClient = new WebClient();
        }

        public async Task CheckSDNForChanges(CancellationToken cancellationToken)
        {
            var currentSdnFilePath = await _systemSettingsService.GetSettingValue(ISystemSettingsService.LastSDNFilePath, cancellationToken);
            Directory.CreateDirectory(_sdnServiceConfiguration.SDNFilesFolder);
            var newSdnFilePath = Path.Combine(_sdnServiceConfiguration.SDNFilesFolder, $"sdn_{DateTime.UtcNow.Ticks}.xml");

            _webClient.DownloadFile(_sdnServiceConfiguration.SourceUrl, newSdnFilePath);

            var newSdnSource = new SDNXmlDataSource(newSdnFilePath);
            using var oldSdnSource = currentSdnFilePath != null ? new SDNXmlDataSource(currentSdnFilePath).GetEnumerator() : null;

            var events = GetSdnEvents(oldSdnSource, newSdnSource.GetEnumerator()).ToList();

            await _dbContext.SDNEntriesEvents.AddRangeAsync(events, cancellationToken);
            await _systemSettingsService.SetSettingValue(ISystemSettingsService.LastSDNFilePath, newSdnFilePath, cancellationToken);
            await _dbContext.SaveChangesAsync(cancellationToken);

            _logger.LogInformation(string.Join(Environment.NewLine, events.Select(x => x.ToString())));
            _logger.LogInformation($"Added: {events.Count(x => x.EventType == EventType.Added)}, {Environment.NewLine}" +
                                   $"Modified: {events.Count(x => x.EventType == EventType.Modified)}, {Environment.NewLine} " +
                                   $"Removed: {events.Count(x => x.EventType == EventType.Modified)}, {Environment.NewLine}");
        }

        //The method is left public for testing
        public IEnumerable<SDNEntryEvent> GetSdnEvents(IEnumerator<SdnEntry> oldSource, IEnumerator<SdnEntry> newSource)
        {
            bool reachedEndOfNewSdnList = !newSource.MoveNext();
            if (oldSource != null)
            {
                while (oldSource.MoveNext())
                {
                    var currentEntry = oldSource.Current;
                    var newEntry = newSource.Current;

                    //According to IDs there's a "gap" in new SDN list, assuming some records have been removed
                    while (currentEntry != null && newEntry != null && currentEntry.Uid < newEntry.Uid || reachedEndOfNewSdnList)
                    {
                        //Entry is removed in new SDN list
                        yield return new SDNEntryEvent(currentEntry.Name, currentEntry.Uid, EventType.Removed);

                        //Reached last entry in old SDN list
                        if (!oldSource.MoveNext())
                            break;

                        currentEntry = oldSource.Current;
                    }

                    //Entry exists in both old and new SDN list
                    if (currentEntry != null && newEntry != null && currentEntry.Uid == newEntry.Uid)
                    {
                        if (!currentEntry.IsDeepEqual(newEntry))
                        {
                            //Entry has been modified 
                            yield return new SDNEntryEvent(newEntry.Name, newEntry.Uid, EventType.Modified);
                        }

                        reachedEndOfNewSdnList = !newSource.MoveNext();
                    }
                }
            }

            while (!reachedEndOfNewSdnList)
            {
                //Newly added item
                if (newSource.Current != null)
                    yield return new SDNEntryEvent(newSource.Current.Name, newSource.Current.Uid, EventType.Added);

                reachedEndOfNewSdnList = !newSource.MoveNext();
            }
        }

        public void Dispose()
        {
            _dbContext?.Dispose();
            _webClient?.Dispose();
        }
    }
}
