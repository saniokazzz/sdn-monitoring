﻿using Microsoft.EntityFrameworkCore;
using SDNMonitoring.Storage.Entities;

namespace SDNMonitoring.Storage
{
    public class SDNEventsDbContext : DbContext
    {
        public DbSet<SystemSetting> SystemSettings { get; set; }
        public DbSet<SDNEntryEvent> SDNEntriesEvents { get; set; }

        public SDNEventsDbContext(DbContextOptions<SDNEventsDbContext>  options) : base(options)
        {
        }
    }
}
