﻿using System.ComponentModel.DataAnnotations.Schema;

namespace SDNMonitoring.Storage.Entities
{
    [Table("SystemSettings")]
    public class SystemSetting : Entity<string>
    {
        public string Value { get; set; }

        protected SystemSetting()
        {
            //Do not remove, it's used by EntityFramework
        }

        public SystemSetting(string id, string value) : base(id)
        {
            Value = value;
        }
    }
}
