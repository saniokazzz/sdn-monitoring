﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SDNMonitoring.Storage.Entities
{
    public abstract class Entity<T>
    {
        [Key]
        [Required]
        public T Id { get; set; }

        protected Entity()
        {
            //Do not remove, it's used by EntityFramework
        }

        protected Entity(T id)
        {
            if (id == null)
                throw new ArgumentException("Id cannot be null.");

            Id = id;
        }
    }
}
