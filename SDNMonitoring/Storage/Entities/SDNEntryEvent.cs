﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SDNMonitoring.Storage.Entities
{

    [Table("SDNEntriesEvents")]
    public class SDNEntryEvent : Entity<Guid>
    {
        public DateTime Timestamp { get; set; }

        public string Name { get; }
        
        public int SourceReference { get; }
        
        public EventType EventType { get; }

        protected SDNEntryEvent()
        {
            //Do not remove, it's used by EntityFramework
        }

        public SDNEntryEvent(string name, int sourceReference, EventType eventType) : base(Guid.NewGuid())
        {
            Timestamp = DateTime.UtcNow;
            Name = name;
            SourceReference = sourceReference;
            EventType = eventType;
        }

        public override string ToString()
        {
            return $"{Name} {SourceReference} {EventType}";
        }
    }

    public enum EventType
    {
        Added = 1,
        Modified = 2,
        Removed = 3
    }
}
