﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using SDNMonitoring.Configuration;
using SDNMonitoring.Services;
using SDNMonitoring.Storage;

namespace SDNMonitoring
{
    /// <summary>
    /// The program can be run both as a console application, also it can be installed as Windows service.
    /// If running as console application, all frequent work logging happens in console and Windows EventLog;
    /// If running as Windows service, service workload can be monitored through Windows EventLog;
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();
            //Uncomment this line, if would like to test first-time import with an empty database 
            //host.Services.GetRequiredService<SDNEventsDbContext>().Database.EnsureDeleted();
            host.Services.GetRequiredService<SDNEventsDbContext>().Database.EnsureCreated();
            host.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureLogging((context, logging) =>
                {
                    if (!System.Diagnostics.EventLog.SourceExists("SDNMonitoring"))
                    {
                        System.Diagnostics.EventLog.CreateEventSource("SDNMonitoring", "SDNMonitoringLog");
                    }
                    
                    logging.ClearProviders();
                    logging.AddConsole();
                    logging.AddEventLog(config =>
                    {
                        config.LogName = "SDNMonitoringLog";
                        config.SourceName = "SDNMonitoring";
                    });
                })
                .ConfigureAppConfiguration(config =>
                {
                    config.AddJsonFile("appsettings.json", false, true);
                })
                .ConfigureServices((hostBuilder, services) =>
                {
                    services.AddHostedService<SDNMonitoringWorkerService>();
                    services.AddScoped<ISystemSettingsService, SystemSettingsService>();
                    services.AddScoped<ISDNMonitoringService, SDNMonitoringService>();
                    services.AddSingleton<SDNServiceConfiguration>();
                    services.AddDbContext<SDNEventsDbContext>(options =>
                    {
                        //Using EF with SQLite to store events, just to demonstrate possibilities
                        options.UseSqlite(hostBuilder.Configuration.GetConnectionString("SDNListEventsDb"));
                    });
                }).UseWindowsService();
    }
}
