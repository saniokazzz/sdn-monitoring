﻿using Microsoft.Extensions.Configuration;

namespace SDNMonitoring.Configuration
{
    public class SDNServiceConfiguration
    {
        public string SourceUrl { get; }
        public string SDNFilesFolder { get; }
        public int MonitoringIntervalSeconds { get; }

        public SDNServiceConfiguration(IConfiguration config)
        {
            SourceUrl = config.GetValue<string>("SDNMonitoringService:SourceUrl");
            SDNFilesFolder = config.GetValue<string>("SDNMonitoringService:SDNFilesFolder");
            MonitoringIntervalSeconds = config.GetValue<int>("SDNMonitoringService:MonitoringIntervalSeconds");
        }
    }
}
