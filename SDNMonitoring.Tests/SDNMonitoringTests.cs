﻿using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using SDNMonitoring.Configuration;
using SDNMonitoring.Services;
using SDNMonitoring.Storage;
using Xunit;

namespace SDNMonitoring.Tests
{
    public class SDNMonitoringTests
    {
        [Fact]
        public void EventsAreProducedForAddedEntries()
        {
            var oldSdnSource = new SDNXmlDataSource(@"TestsInput\NewEntries\old.xml");
            var newSdnSource = new SDNXmlDataSource(@"TestsInput\NewEntries\new.xml");

            var sut = GetMonitoringServiceInstance();
            var result = sut.GetSdnEvents(oldSdnSource.GetEnumerator(), newSdnSource.GetEnumerator()).ToList();

            Assert.Equal(3, result.Count);
            result.Select(x => x.ToString()).Should().Contain(new List<string>()
            {
                "Amir Muhammad Sa'id Abdal-Rahman AL-MAWLA 28717 Added",
                "Amir DIANAT 28859 Added",
                "TAIF MINING SERVICES LLC 28860 Added"
            });
        }

        [Fact]
        public void EventsAreProducedForRemovedEntries()
        {
            var oldSdnSource = new SDNXmlDataSource(@"TestsInput\RemovedEntries\old.xml");
            var newSdnSource = new SDNXmlDataSource(@"TestsInput\RemovedEntries\new.xml");
            var sut = GetMonitoringServiceInstance();
            var result = sut.GetSdnEvents(oldSdnSource.GetEnumerator(), newSdnSource.GetEnumerator()).ToList();

            Assert.Equal(5, result.Count);
            result.Select(x => x.ToString()).Should().Contain(new List<string>()
            {
                "AEROCARIBBEAN AIRLINES 36 Removed",
                "IRAN & SHARGH COMPANY 16020 Removed",
                "Stanislav Anatolyevich VOROBYEV 28704 Removed",
                "Denis Valiullovich GARIYEV 28705 Removed",
                "Nikolay Nikolayevich TRUSHCHALOV 28706 Removed"
            });
        }

        [Fact]
        public void EventsAreProducedForModifiedEntries()
        {
            var oldSdnSource = new SDNXmlDataSource(@"TestsInput\ModifiedEntries\old.xml");
            var newSdnSource = new SDNXmlDataSource(@"TestsInput\ModifiedEntries\new.xml");
            var sut = GetMonitoringServiceInstance();
            var result = sut.GetSdnEvents(oldSdnSource.GetEnumerator(), newSdnSource.GetEnumerator()).ToList();

            Assert.Equal(3, result.Count);
            result.Select(x => x.ToString()).Should().Contain(new List<string>()
            {
                "CIMEX 537 Modified",
                "Abubakar Adam KAMBAR 15010 Modified",
                "Nikolay Nikolayevich TRUSHCHALOV 28706 Modified"
            });
        }

        [Fact]
        public void EventsOfAllTypesAreProduced()
        {
            var oldSdnSource = new SDNXmlDataSource(@"TestsInput\AllEventTypes\old.xml");
            var newSdnSource = new SDNXmlDataSource(@"TestsInput\AllEventTypes\new.xml");
            var sut = GetMonitoringServiceInstance();
            var result = sut.GetSdnEvents(oldSdnSource.GetEnumerator(), newSdnSource.GetEnumerator()).ToList();

            Assert.Equal(9, result.Count);
            result.Select(x => x.ToString()).Should().Contain(new List<string>()
            {
                "Khadafi Abubakar JANJALANIK 8807 Modified",
                "KOREA DAESONG BANK 12312 Modified",
                "Jorge Andres CIFUENTES OSORIO 12316 Removed",
                "Teresa de Jesus CIFUENTES VILLA 12318 Removed",
                "Bassam Ahmad AL-HASRI 21265 Removed",
                "Iyad Nazmi Salih KHALIL 21266 Modified",
                "Stanislav Anatolyevich VOROBYEV 28704 Added",
                "Denis Valiullovich GARIYEV 28705 Added",
                "Nikolay Nikolayevich TRUSHCHALOV 28706 Added"
            });
        }

        public SDNMonitoringService GetMonitoringServiceInstance()
        {
            var config = new ConfigurationBuilder().Build();
            var loggerMock = new Mock<ILogger<SDNMonitoringService>>();
            var systemSettingsServiceMock = new Mock<ISystemSettingsService>();
            return new SDNMonitoringService(new SDNServiceConfiguration(config), systemSettingsServiceMock.Object,
                loggerMock.Object, new SDNEventsDbContext(new DbContextOptions<SDNEventsDbContext>()));
        }
    }
}
